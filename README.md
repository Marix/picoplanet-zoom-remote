PicoPlanet as a Zoom Remote
===========================

This configures the [PicoPlanet](https://bleeptrack.de/projects/picoplanet/) as a simple remote for Zoom.

* Button 1, the one next to the USB Port, will toggle audio.
* Button 2, the middle one, will toggle video.

The shortcuts send are currently the ones for OS X.

Suggested Configuration
-----------------------

To make this work even if Zoom is not the currently active window on your system, you should configure the "Toggle Audio" and "Toggle Video" shortcuts to be global in the Zoom settings.

Installation
------------

This requires the Adafruit HID Library from <https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases>.
Follow the instructions to install it on your PicoPlanet.
Afterwards, copy the `code.py` file onto your PicoPlanet and enjoy.

License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.