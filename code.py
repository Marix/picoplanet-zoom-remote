import board
import time
import usb_hid
import touchio
from digitalio import DigitalInOut, Direction, Pull

from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode

TOUCH_THRESHOLD_RAW = 2500
BOUNCE_PROTECTION_TIME_S = 0.2

touch1 = touchio.TouchIn(board.A0)
touch2 = touchio.TouchIn(board.A1)
touch3 = touchio.TouchIn(board.A2)

led_red = DigitalInOut(board.D6)
led_red.direction = Direction.OUTPUT
led_green = DigitalInOut(board.D5)
led_green.direction = Direction.OUTPUT
led_blue = DigitalInOut(board.D7)
led_blue.direction = Direction.OUTPUT

time.sleep(1)
keyboard = Keyboard(usb_hid.devices)

led_red.value = True
led_green.value = True
led_blue.value = False

print("Zoom Remote started")

while True:
    if touch1.raw_value > TOUCH_THRESHOLD_RAW:
        keyboard.send(Keycode.GUI, Keycode.SHIFT, Keycode.A)
        time.sleep(BOUNCE_PROTECTION_TIME_S)

    if touch2.raw_value > TOUCH_THRESHOLD_RAW:
        keyboard.send(Keycode.GUI, Keycode.SHIFT, Keycode.V)
        time.sleep(BOUNCE_PROTECTION_TIME_S)
